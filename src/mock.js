import fetchMock from "fetch-mock"

import deformation_response from './data/deformation_response.json'
import termo_response from './data/termo_response.json'
import deformation_trend_response from './data/deformation_trend_response.json'
import termo_trend_response from './data/termo_trend_response.json'

const mockData = {
    deformation: {
        trend: deformation_trend_response,
        default: deformation_response
    },
    termo: {
        trend: termo_trend_response,
        default: termo_response
    }
}

fetchMock.get(/\/api\/measurements\/trend\/(.+)/, (url,opt,pp)=>{
    let id = url.split("/")[4];
    return {body: mockData[id]["trend"]};
});

fetchMock.get(/\/api\/measurements\/(.+)/, (url,opt,pp)=>{
    let id = url.split("/")[3];
    return {body: mockData[id]["default"]};
});

/*

fetch("/api/measurements/trend/termo")
.then(async data=>{
    let p = await data.json();
    debugger;
});*/
