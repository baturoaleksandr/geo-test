import './App.css';
import ThermokosaTable from "./components/ThermokosaTable";
import DefeormTable from "./components/DeformTable";
import RangeDatePicker from "./components/RangeDatePicker";
import React, {useState} from "react";
import "tabulator-tables/dist/css/tabulator.min.css";
import "tabulator-tables/dist/css/tabulator_midnight.min.css"

function App() {
  const [period, setPeriod] = useState();

  return (
      <div className="App">
          <div><RangeDatePicker onChange={(period)=>setPeriod(period)}/></div>
          <ThermokosaTable {...period} />
          <DefeormTable {...period} />
      </div>
  );
}

export default App;
