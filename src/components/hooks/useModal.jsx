import {useState} from "react";
import Modal from "react-modal";

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
    },
};

Modal.setAppElement('#modal');

export default function useModal(){
    const [modalIsOpen, setIsOpen] = useState(false);

    function openModal() {
        setIsOpen(true);
    }

    function afterOpenModal() {
    }

    function closeModal() {
        setIsOpen(false);
    }

    return {
        openModal: openModal.bind(this),
        isOpen: modalIsOpen,
        onAfterOpen: afterOpenModal.bind(this),
        onRequestClose: closeModal.bind(this),
        style: customStyles,
        contentLabel: "Chart"
    }
}