import React, {useState} from "react";
import DatePicker from "react-date-picker";
import 'react-date-picker/dist/DatePicker.css';
import 'react-calendar/dist/Calendar.css';

export default function RangeDatePicker({onChange}){
    const [startDate, setStartDate] = useState();
    const [endDate, setEndDate] = useState();

    return <div>
        <label>C </label>
        <DatePicker value={startDate} onChange={
            (date) => {
                setStartDate(date);
                onChange && onChange({
                    startDate:date,
                    endDate
                })
            }
        }/>
        <label>По </label>
        <DatePicker value={endDate} onChange={
            (date) => {
                setEndDate(date);
                onChange && onChange({
                    startDate,
                    endDate:date
                })
            }
        }/>
    </div>;
}