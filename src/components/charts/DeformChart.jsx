import Plotly from 'plotly.js-dist-min'
import {useEffect, useMemo, useRef} from "react";

export default function DeformChart({data = [],trendData=[],endEkspl=new Date("01.12.2025")}){
    const ref = useRef();
    const {layout,plotData} = useMemo(() => {
        let dX = [],
            dY = [];

        data.forEach(el=>{
            if (el.data.delta!==undefined) {
                dY.push((el.data.delta || 0) * 1);
                dX.push(new Date(el.time));
            }
        })

        const maxDelta = Math.max(...dY);
        const minDelta = Math.min(...dY);

        const delta = {
            x: dX,
            y: dY,
            mode: 'lines',
            name: 'Δ'
        };

        let dXTrend = [],
            dYTrend = [];

        Object.keys(trendData).forEach(key=>{
            const value = trendData[key];
            if (value!==undefined) {
                dXTrend.push(new Date(key));
                dYTrend.push(value);
            }
        })

        const trend = {
            x: dXTrend,
            y: dYTrend,
            mode: 'lines',
            name: 'Тренд Δ'
        }

        const layout = {
            xaxis: {
                title: 'Дата'
            },
            yaxis: {
                title: 'Смещение (Δ),м'
            },
            height: 600,
            showlegend: true,
            shapes: [
                {
                    type: 'line',
                    xref: 'x',
                    yref: 'paper',
                    x0: endEkspl,
                    y0: 0,
                    x1: endEkspl,
                    y1: 1,
                    showlegend: true,
                    name: "Конец эксплуатации",
                    line: {
                        color: 'rgb(90, 90, 96)',
                        width: 2
                    }
                },
                {
                    type: 'line',
                    xref: 'paper',
                    yref: 'y',
                    x0: 0,
                    y0: maxDelta,
                    x1: 1,
                    y1: maxDelta,
                    showlegend: true,
                    name: "Макс Δ, м",
                    line: {
                        color: 'rgb(50, 171, 96)',
                        width: 2,
                        dash: 'dashdot'
                    }
                },
                {
                    type: 'line',
                    xref: 'paper',
                    yref: 'y',
                    x0: 0,
                    y0: minDelta,
                    x1: 1,
                    y1: minDelta,
                    showlegend: true,
                    name: "Мин Δ, м",
                    line: {
                        color: 'rgb(171, 90, 96)',
                        width: 2,
                        dash: 'dashdot'
                    }
                }]
        }
        const plotData = [ delta, trend];
        return {
            layout,
            plotData
        }
    }, [data,trendData,endEkspl]);
    useEffect(() => {
        Plotly.newPlot(ref.current, plotData, layout);
    }, [layout,plotData]);

    return <div ref={ref}/>
}