import React, {useEffect, useMemo, useRef, useState} from "react";
import {TabulatorFull as Tabulator} from 'tabulator-tables';
import moment from "moment";
import DeformChart from "./charts/DeformChart";
import useModal from "./hooks/useModal";
import Modal from 'react-modal';

let columns = [
    {
        title: 'Дата и время измерения',
        field: 'time',
        //sorter: 'date',
        formatter: cell => moment(cell.getValue())
            .format(`DD.MM.YYYY HH:mm`)
    },
    {
        title: 'Цикл измерения',
        headerSort: false,
        field: 'num',
        formatter: cell => +cell.getValue().toFixed(2)
    },
    {
        title: 'Отметка,м',
        headerSort: false,
        field: 'data',
        formatter: cell => (cell.getValue()["value"]*1).toFixed(4)
    },
    {
        title: 'Δ,м',
        headerSort: false,
        field: 'data',
        formatter: cell => {
            let obj = cell.getValue();
            return obj['delta']?(obj['delta']*1).toFixed(4):""
        }
    },

];
function useData(){
    const [data, setData] = useState();
    if (!data){
        fetch("/api/measurements/deformation")
            .then(async data=>{
                const dat = (await data.json())?.data;
                dat.sort((el1,el2)=>{
                        return moment(el1.time)>moment(el2.time)?1:-1
                })
                dat.forEach(
                    (el,ind)=>{
                        el.num = ind+1
                    }
                )
                setData(dat);
            })
    }
    return data;
}
function useTrendData(){
    const [data, setData] = useState();
    if (!data){
        fetch("/api/measurements/trend/deformation")
            .then(async data=>{
                const dat = await data.json();
                setData(dat);
            })
    }
    return data;
}

export default function DeformTable({startDate,endDate}){
    const data = useData();
    const trendData = useTrendData();
    const ref = useRef(null);
    const tabulatorRef = useRef(null);
    const {openModal,...modalProps} = useModal();

    const filteredData  = useMemo(() => {
        if (!data)
            return null;

        return data.filter(el=>{
            if (startDate && moment(el.time)<moment(startDate))
                return false;
            if (endDate && moment(el.time)>moment(endDate))
                return false;
            return true;
        })
    }, [data,startDate,endDate]);

    useEffect(() => {
        if (filteredData && tabulatorRef.current){
            tabulatorRef.current.setData(filteredData);
        }else{
            tabulatorRef.current = new Tabulator(ref.current, {
                height: "350px",
                reactiveData: true, //enable data reactivity
                columns: columns,
                data: filteredData
            });
        }
    }, [filteredData]);



    return <>
        <Modal {...modalProps}>
            <DeformChart data={filteredData} trendData={trendData?.points}/>
        </Modal>
        <div align="left">Деформационная марка, dm5
            <button onClick={openModal}>График</button>
        </div>
        <div ref={ref}/>
    </>
}