import React, {useEffect, useMemo, useRef, useState} from "react";
import {TabulatorFull as Tabulator} from 'tabulator-tables';
import moment from "moment";


let columns = [
    {
        title: 'Дата и время измерения',
        field: 'time',
        frozen: true,
        formatter: cell => moment(cell.getValue())
            .format(`DD.MM.YYYY HH:mm`)
    },
    {
        title: 'Те',
        frozen: true,
        headerSort: false,
        field: 'averageTemperature',
        formatter: cell => +cell.getValue().toFixed(2)
    },

];
function useData(){
    const [data, setData] = useState();
    if (!data){
        fetch("/api/measurements/termo")
            .then(async data=>{
                const dat = await data.json();
                setData(dat.data);
            })
    }
    return data;
}
function getDepthColumns(data){
    // collect all available depth values
    let columns = [];
    let depthArr = new Set();
    data.forEach(el=>{
        Object.keys(el.data).forEach(depth=>{
            depthArr.add(depth);
        })
    })
    depthArr = [...depthArr]
    depthArr.sort((el1,el2)=>+el1>+el2?1:-1);

    // generate columns for depths
    depthArr.forEach((el)=>{
        columns.push( {
            title: el,
            field: 'data',
            headerSort: false,
            formatter: cell => {
                let obj = cell.getValue();
                return obj[el]?(obj[el].value*1).toFixed(2):""
            }
        })
    })

    // return column group
    return [
        {
            title:"Глубина, м",
            columns: columns,
        }]
}

export default function ThermokosaTable({startDate,endDate}){
    const data = useData();
    const ref = useRef(null);
    const tabulatorRef = useRef(null);
    const filteredData  = useMemo(() => {
        if (!data)
            return null;

        return data.filter(el=>{
            if (startDate && moment(el.time)<moment(startDate))
                return false;
            if (endDate && moment(el.time)>moment(endDate))
                return false;
            return true;
        })
    }, [data,startDate,endDate]);

    useEffect(() => {
        if (filteredData && tabulatorRef.current) {
            tabulatorRef.current.setData(filteredData);
        }else {
            if (data) {
                const depthColumns = getDepthColumns(data);

                tabulatorRef.current = new Tabulator(ref.current, {
                    height: "350px",
                    rowHeader: {headerSort: false, hozAlign: "center", resizable: false},
                    data: data, //link data to table
                    reactiveData: true, //enable data reactivity
                    columns: [
                        ...columns,
                        ...depthColumns
                    ],
                });
            }
        }
    }, [data,filteredData]);

    return <>
        <div align="left">Термометрическая скважина, ts3</div>
        <div ref={ref}/>
    </>
}